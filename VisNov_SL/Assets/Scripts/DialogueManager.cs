using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    [SerializeField] private AudioSource sfxSource;
    [SerializeField, Header("Variablen f�r eine Coole Designerin!<3"), Tooltip("Dialogues to play")] private Dialogue[] dialogues;
    [SerializeField, Range(0, 0.5f), Tooltip("Speed of text 'generation'")] private float textSpeed = 0.05f;
    [SerializeField] private EndScene end;

    [SerializeField, Header("Dev Stuff, am Besten nicht ver�ndern!:D")] private string debugString;
    [SerializeField] private TextMeshProUGUI textLeft, textRight, textMid;
    [SerializeField] private TextMeshProUGUI nameL, nameR, nameM;
    [SerializeField] private BoxMaster boxMaster;
    [SerializeField] private Image backGround, charL, charM, charR;
    [SerializeField] private AudioSource source;
    [SerializeField] private DecisionHandler up, mid, down;
    [SerializeField] private GameObject decisions;
    [SerializeField] private Image fader;
    private Color colorDef, colorTransparent;

    //Intern stuff
    private TextMeshProUGUI text;   //current text
    private TextPlace placing;
    private int n;  //used to determine index
    private int sentence_i, dialogue_i;
    private string currentStringToType;
    private float timer;
    private bool isFull;
    private float coolDownTimer;
    private float originalVolume;

    private static DialogueManager instance;

    private void Start()
    {
        originalVolume = source.volume + 0.01f;
        colorDef = new Color(0, 0, 0, 1);
        colorTransparent = new Color(0, 0, 0, 0);
        fader.color = colorDef;
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Debug.LogError("There should only be one dialogue manager!:D Your second instance is: " + gameObject + " ,Please delete :>");
            this.enabled = false;
        }
        placing = dialogues[0].place;

        ChangeCurrentText(placing);
        boxMaster.SwitchDialogues(DialogueIndex(placing));
        ChangeNameText(placing);

        text.text = "";
        currentStringToType = dialogues[dialogue_i].sentences[sentence_i];
        timer = 0;

        StartCoroutine(Intro());
    }

    private void Update()
    {
        if(!isFull) AddToString(currentStringToType);
        else
        {
            if(coolDownTimer < 0)
            {
                if (Input.anyKeyDown && !decisions.activeInHierarchy) NextDialogueOrSentence();
            }
            else
            {
                coolDownTimer -= Time.deltaTime;
            }
        }
    }


    private void AddToString(string input)
    {
        if (Input.anyKeyDown)
        {
            text.text = input;
            isFull = true;
        }

        if(timer < textSpeed)
        {
            timer += Time.deltaTime;
        }
        else
        {
            if(n < input.Length)
            {
                text.text += input[n];
                n++;
            }
            else
            {
                isFull = true;
            }
            timer = 0;
        }
    }

    private int DialogueIndex(TextPlace input)
    {
        switch (input)
        {
            case TextPlace.decision:        //more like non lmao
                return 0;
                

            case TextPlace.left:
                return 1;
                

            case TextPlace.right:
                return 1;


            case TextPlace.mid:
                return 2;
        }
        Debug.LogWarning("If you see this message than bruh tf happened, there should be no way to see this xD");
        return 0;
    }


    //This here is the stuff that gets executed "once" on a frame and not continious^^
    private void NextDialogueOrSentence()
    {
        //the catch should only happen when the index exceeds the array. Yes im lazy :D
        try
        {
            if (sentence_i == dialogues[dialogue_i].sentences.Length - 1)
            {

                sentence_i = 0;
                dialogue_i++;
                placing = dialogues[dialogue_i].place;

                ChangeCurrentText(placing);
                boxMaster.SwitchDialogues(DialogueIndex(placing));
                ChangeNameText(placing);

                coolDownTimer = 1.5f;   //1.5f Sekunden cooldown
            }
            else
            {                
                sentence_i++;

                //Play Sfx if current sentence index matches set index
                if(dialogues[dialogue_i].soundEffect != null){
                    if(sentence_i == (int)dialogues[dialogue_i].soundEffectIndex){
                        sfxSource.clip = dialogues[dialogue_i].soundEffect;
                        sfxSource.Play();
                    }
                }
            }
            currentStringToType = dialogues[dialogue_i].sentences[sentence_i];

            n = 0;
            isFull = false;
        }
        catch
        {
            if (end.decision)
            {
                decisions.SetActive(true);

                up.ChangeText(end.upperDecisionText);
                mid.ChangeText(end.midDecisionText);
                down.ChangeText(end.lowerDecisiontext);

                up.output = end.upperScene;
                mid.output = end.midScene;
                down.output = end.lowerScene;
            }
            else
            {
                if (end.endsTheGame)
                {
                    StartCoroutine(Outro());                   
                }
                else
                {
                    StartCoroutine(FadeOutAndChange(end.nextScene));
                }
            }
        }

        text.text = "";
    }



    private void ChangeCurrentText(TextPlace input)
    {
        //reset before action
        if(text != null) text.text = "";
        switch (input)
        {
            case TextPlace.decision:
                //text = textLeft;
                Debug.LogWarning("Decision not implemented yet");
                break;

            case TextPlace.left:
                text = textLeft;

                textMid.text = "";
                textRight.text = "";
                break;

            case TextPlace.right:
                text = textRight;

                textMid.text = "";
                textLeft.text = "";
                break;

            case TextPlace.mid:
                text = textMid;

                textRight.text = "";
                textLeft.text = "";
                break;
        }
    }


    private void ChangeNameText(TextPlace input)
    {
        switch (input)
        {
            case TextPlace.decision:
                nameL.text = "";
                nameR.text = "";
                nameM.text = "";
                break;

            case TextPlace.left://For some god damn reason this is inverted but i have to be fast unfortunately so...yeah a temporary solution xD
                charR.sprite = dialogues[dialogue_i].sprite;
                nameR.text = dialogues[dialogue_i].characterName;

                if (dialogues[dialogue_i].secondSprite != null)
                {
                    charL.sprite = dialogues[dialogue_i].secondSprite;
                }

                if (dialogues[dialogue_i].secondString != "")
                {
                    nameL.text = dialogues[dialogue_i].secondString;
                }
                break;

            case TextPlace.right:
                charL.sprite = dialogues[dialogue_i].sprite;
                nameL.text = dialogues[dialogue_i].characterName;

                if(dialogues[dialogue_i].secondSprite != null)
                {
                    charR.sprite = dialogues[dialogue_i].secondSprite;
                }

                if (dialogues[dialogue_i].secondString != "")
                {
                    nameR.text = dialogues[dialogue_i].secondString;
                }
                break;

            case TextPlace.mid:
                charM.sprite = dialogues[dialogue_i].sprite;
                nameM.text = dialogues[dialogue_i].characterName;
                break;
        }
    }

    public void InitFadeOutAndChange(Mine.Scene input)
    {
        decisions.SetActive(false);
        StartCoroutine(FadeOutAndChange(input));
        StartCoroutine(SoundFadeOut(input));
    }

    public IEnumerator FadeOutAndChange(Mine.Scene input)
    {
        if (!input.useFadeout)
        {
            LoadNewScene(input);
            yield break;
        }
        coolDownTimer = 1.5f;   //1.5f Sekunden cooldown
        while(fader.color != colorDef)
        {
            fader.color = Color.Lerp(fader.color, colorDef, 0.5f * Time.deltaTime * 50);
            yield return new WaitForSecondsRealtime(0.001f);
        }
        coolDownTimer = 1.5f;   //1.5f Sekunden cooldown
        LoadNewScene(input);
        yield return new WaitForSecondsRealtime(1f);

        while (fader.color != colorTransparent)
        {
            fader.color = Color.Lerp(fader.color, colorTransparent, 0.3f * Time.deltaTime * 50);
            yield return new WaitForSecondsRealtime(0.001f);
        }
        //fade screen out
    }

    public IEnumerator Intro()
    {
        while (fader.color != colorTransparent)
        {
            fader.color = Color.Lerp(fader.color, colorTransparent, 0.1f * Time.deltaTime * 50);
            yield return new WaitForSecondsRealtime(0.001f);
        }
    }

    public IEnumerator Outro()
    {
        while (fader.color != colorDef)
        {
            fader.color = Color.Lerp(fader.color, colorDef, 0.15f * Time.deltaTime * 50);
            yield return new WaitForSecondsRealtime(0.0001f);
        }

        SceneManager.LoadScene("GameEnd");
    }

    private void LoadNewScene(Mine.Scene input)
    {
        coolDownTimer = 1.5f;   //1.5f Sekunden cooldown
        isFull = true;
        backGround.sprite = input.backGround;
        dialogues = input.dialogues;
        end = input.end;


        dialogue_i = 0;
        sentence_i = 0;
        n = 0;

        placing = dialogues[0].place;

        ChangeCurrentText(placing);
        boxMaster.SwitchDialogues(DialogueIndex(placing));
        ChangeNameText(placing);

        currentStringToType = dialogues[dialogue_i].sentences[sentence_i];
        isFull = false;
    }

    private IEnumerator SoundFadeOut(Mine.Scene input)
    {
        while(source.volume > 0)
        {
            source.volume -= Time.deltaTime * 20;
            yield return new WaitForSecondsRealtime(0.01f);
        }

        source.Stop();
        Debug.Log(input);
        source.clip = input.ambience;
        source.Play();

        while (source.volume < originalVolume)
        {
            source.volume += Time.deltaTime * 20;
            yield return new WaitForSecondsRealtime(0.01f);
        }
    }
}
