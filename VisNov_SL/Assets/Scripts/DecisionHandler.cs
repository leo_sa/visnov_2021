using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DecisionHandler : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    public Mine.Scene output;
    private DialogueManager manager;
    private void Awake()
    {
        manager = FindObjectOfType<DialogueManager>(); //there should always be only one instance
    }

    public void Init()
    {
        if(output != null)
        {
            manager.InitFadeOutAndChange(output);
        }
    }

    public void ChangeText(string input)
    {
        text.text = input;
    }
}
