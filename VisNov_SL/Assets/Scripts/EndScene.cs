using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Visual_Novel/EndScene", order = 2)]
public class EndScene : ScriptableObject
{
    [Tooltip("Is the End of this scene a Decision?")]public bool decision;
    [Header("Varaibles if not a decision"), Tooltip("Next Scene to load")] public Mine.Scene nextScene;
    [Tooltip("Roll credits after this!")] public bool endsTheGame;
    [Header("Variables if it is a decision"), Tooltip("The text on the decision buttons")] public string upperDecisionText;
    [Tooltip("The text on the decision buttons")] public string midDecisionText, lowerDecisiontext;
    [Tooltip("Scene to load when taking decisions. Leave blank to 'disable'(make unfunctional) the specific button")] public Mine.Scene upperScene, midScene, lowerScene;

}
