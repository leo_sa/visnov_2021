using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxMaster : MonoBehaviour
{
    [SerializeField, Header("Auch nicht ver�ndern, bitte!:D")] private RectTransform boxR, boxL, boxM;
    private float outsideOffset = 2000;
    private Vector3 posR, posL, posM;   //Original localPositions
    private Vector3 tarPosR, tarPosL, tarPosM;  //target localPositions when lerping out
    private Vector3 curPosR, curPosL, curPosM;  //Current localPositions to lerp
    private int activeIndex;    //0 = none, 1 = LR, 2 = M
    public int ActiveIndex
    {
        //get { return activeIndex; }
        set { activeIndex = value; }
    }
    
    void Awake()
    {
        posR = boxR.localPosition;
        posL = boxL.localPosition;
        posM = boxM.localPosition;

        tarPosR = new Vector3(boxR.localPosition.x + outsideOffset, boxR.localPosition.y, boxR.localPosition.z);
        tarPosL = new Vector3(boxL.localPosition.x - outsideOffset, boxL.localPosition.y, boxL.localPosition.z);
        tarPosM = new Vector3(boxM.localPosition.x, boxM.localPosition.y - outsideOffset, boxM.localPosition.z);

        //SwitchDialogues(0);
    }

    // Update is called once per frame
    void Update()
    {
        if (boxR.localPosition != curPosR) boxR.localPosition = Vector3.Lerp(boxR.localPosition, curPosR, 0.05f * Time.deltaTime * 50);   
        if (boxL.localPosition != curPosL) boxL.localPosition = Vector3.Lerp(boxL.localPosition, curPosL, 0.05f * Time.deltaTime * 50);   
        if (boxM.localPosition != curPosM) boxM.localPosition = Vector3.Lerp(boxM.localPosition, curPosM, 0.05f * Time.deltaTime * 50);   
    }

    public void SwitchDialogues(int input)
    {
        switch (input)
        {
            case 0:
                None();
                break;

            case 1:
                LR();
                break;

            case 2:
                Mid();
                break;
        }
    }

    private void None()
    {
        curPosR = tarPosR;
        curPosL = tarPosL;
        curPosM = tarPosM;
    }

    private void LR()
    {
        curPosR = posR;
        curPosL = posL;
        curPosM = tarPosM;
    }

    private void Mid()
    {
        curPosR = tarPosR;
        curPosL = tarPosL;
        curPosM = posM;
    }
}
