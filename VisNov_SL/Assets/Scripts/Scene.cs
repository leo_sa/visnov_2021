using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Mine
{
    [CreateAssetMenu(fileName = "Data", menuName = "Visual_Novel/Scene", order = 3)]
    public class Scene : ScriptableObject
    {
        [Tooltip("Die Dialoge welche in der Scene gegeben sein werden")] public Dialogue[] dialogues;
        [Tooltip("Das Ende der jeweiligen Scene")] public EndScene end;
        [Tooltip("Der Hintergrund der jeweiligen Scene")] public Sprite backGround;
        [Tooltip("Der Ambient-Sound der jeweiligen Scene")] public AudioClip ambience;
        [Tooltip("Uses a fadeout or direct change?")] public bool useFadeout = true;
    }
}

