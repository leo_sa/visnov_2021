using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TextPlace
{
    right,
    left,
    mid,
    decision
}
[CreateAssetMenu(fileName = "Data", menuName = "Visual_Novel/Dialogue", order = 1)]
public class Dialogue : ScriptableObject
{
    [Tooltip("The Place of the text")]public TextPlace place;
    [Tooltip("The sentences the character shall say")]public string[] sentences;
    [Tooltip("The name of the character")] public string characterName;
    [Tooltip("The visual representation of the character")] public Sprite sprite;
    [Header("Optional"), Tooltip("The sprite of the second character")]public Sprite secondSprite;
    [Tooltip("The name of the second character")] public string secondString;
    [SerializeField, Tooltip("This will be played on the assigned index based on the string[]")] public uint soundEffectIndex;
    [SerializeField] public AudioClip soundEffect;
}
